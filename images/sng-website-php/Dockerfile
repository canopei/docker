FROM php:7.1-fpm-alpine

# install dev utils
RUN apk add --update --no-cache git openssh bash curl vim nodejs build-base autoconf icu-dev postfix pcre-dev

RUN docker-php-ext-install opcache \
  && pecl install xdebug \
  && docker-php-ext-enable xdebug \
  && printf "\n" | pecl install apcu \
  && docker-php-ext-enable apcu \
  && docker-php-ext-install intl

RUN cd /tmp && wget http://getcomposer.org/composer.phar \
    && cp /tmp/composer.phar /usr/bin/composer \
    && chmod +x /usr/bin/composer

RUN mkdir -p /opt/yarn && \
  curl -sL https://yarnpkg.com/latest.tar.gz | tar xz -C /opt/yarn --strip-component 1 && ls -al /opt/ && \
  ln -s /opt/yarn/bin/yarn /usr/local/bin

COPY resources/php.ini /usr/local/etc/php/
COPY resources/docker-php-entrypoint /usr/local/bin/

EXPOSE 8080

ENTRYPOINT ["docker-php-entrypoint"]
CMD ["php-fpm"]