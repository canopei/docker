#!/bin/bash

# docker id might be given as a parameter
INAME=$1

if [[ "$INAME" == "" ]]; then
  echo "The image name is required."
  exit 1
fi

DID=$(docker ps | grep "$INAME" | grep -Eo "^[0-9a-z]{8,}\b" | head -n 1)
if [[ "$DID" == "" ]]; then
  echo "Invalid image name."
  exit 2
fi

docker exec -i -t $DID /bin/sh
