# SNG - Dev environment
=======================

## Requirements
- Docker 1.13.0+

## Setup steps
- (OS X only) Setup d4m-nfs
    - Remove all the file sharing from Docker except `/tmp`
    - `cp ./d4m-nfs/d4m-nfs-mounts.txt.dist ./d4m-nfs/etc/d4m-nfs-mounts.txt`
    - Run `./d4m-nfs/d4m-nfs.sh`

## Misc
- Docker - list all running containers - `docker ps`
- Docker - sh into a container - `docker exec -it [container ID] /bin/sh`
- Docker - stop all containers - `docker stop $(docker ps -a -q)`

## Build and push the godev image
- `docker build -t us.gcr.io/eastern-augury-170215/sng-godev .`
- `gcloud docker -- push us.gcr.io/eastern-augury-170215/sng-godev`